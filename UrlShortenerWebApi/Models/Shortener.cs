﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UrlShortener.Models
{
    public class Shortener
    {
        public Shortener()
        {
            CreateTime = DateTime.Now;
        }

        [Key]
        public int ShortenerId { get; set; }

        [Display(Name = "Kısaltmak İstediğiniz Url"), DataType(DataType.Url, ErrorMessage = "Lütfen geçerli bir url giriniz.")]
        public string LongUrl { get; set; }

        [Display(Name = "Kısa Url'niz")]
        public string ShortUrl { get; set; }

        public DateTime CreateTime { get; set; }
    }
}