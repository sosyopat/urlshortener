﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UrlShortenerWebApi.Models
{
    public class UrlModel
    {
        public int  id { get; set; }
        public string uzunUrl { get; set; }
        public string kisaUrl { get; set; }
        public DateTime olusturmaTarihi { get; set; }
    }
}