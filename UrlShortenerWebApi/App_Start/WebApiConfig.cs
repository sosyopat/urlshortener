﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace UrlShortenerWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();


            // Web API routes
            config.MapHttpAttributeRoutes();
            /*
             * Config yapısına '{action}' yapısını ekledim ki controller içerisinde hangi metodu çağıracağını da url de belirtsin.
             */
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
