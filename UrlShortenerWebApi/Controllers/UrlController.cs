﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UrlShortener.Models;
using UrlShortenerWebApi.Models;

namespace UrlShortenerWebApi.Controllers
{
    public class UrlController : ApiController
    {
        private SiteContext db = new SiteContext();


        /*
         * Metodun isminin başındaki 'Get' eki metodun turunun get metodu olduğunu belirliyor.
         * Eğer metodun isminin başında 'Get' takısı yoksa metodun turunu '[HttpGet]' attribute ile belirtmemiz gerekiyor.
         */
        //[HttpGet]
        public List<UrlModel> GetShortenersAllList()
        {
            List<UrlModel> tumUrlListe = db.Shortener.Select(m => new UrlModel()
            {
                id = m.ShortenerId,
                uzunUrl = m.LongUrl,
                kisaUrl = m.ShortUrl,
                olusturmaTarihi = m.CreateTime
            }).ToList();

            return tumUrlListe;
        }

        [HttpGet]
        public UrlModel urlModel(int id)
        {
            Shortener sModel = db.Shortener.Find(id);
            UrlModel uModel = new UrlModel();
            uModel.kisaUrl = sModel.ShortUrl;
            uModel.uzunUrl = sModel.LongUrl;
            uModel.olusturmaTarihi = sModel.CreateTime;
            return uModel;
        }
    }
}
